# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.38](https://gitlab.com/saastack/ui/departments/compare/v0.1.37...v0.1.38) (2021-08-27)


### Bug Fixes

* Unicode encoder change and packages update ([33fbcff](https://gitlab.com/saastack/ui/departments/commit/33fbcfff87fe1788b220739a2982806b4f3811ef))

### [0.1.37](https://gitlab.com/saastack/ui/departments/compare/v0.1.36...v0.1.37) (2021-08-17)


### Features

* add aliases in department ([dd3ad8d](https://gitlab.com/saastack/ui/departments/commit/dd3ad8dfe3afa90ca70d799df46e1e52e3e771c6))

### [0.1.36](https://gitlab.com/saastack/ui/departments/compare/v0.1.35...v0.1.36) (2021-07-16)

### [0.1.35](https://gitlab.com/saastack/ui/departments/compare/v0.1.34...v0.1.35) (2021-07-03)


### Bug Fixes

* accessibility ([ff293fe](https://gitlab.com/saastack/ui/departments/commit/ff293fed7705db9478a4014b0235d15e42793f2f))

### [0.1.34](https://gitlab.com/saastack/ui/departments/compare/v0.1.33...v0.1.34) (2021-06-29)

### [0.1.33](https://gitlab.com/saastack/ui/departments/compare/v0.1.32...v0.1.33) (2021-06-29)

### [0.1.32](https://gitlab.com/saastack/ui/departments/compare/v0.1.31...v0.1.32) (2021-06-29)

### [0.1.31](https://gitlab.com/saastack/ui/departments/compare/v0.1.30...v0.1.31) (2021-04-10)

### [0.1.30](https://gitlab.com/saastack/ui/departments/compare/v0.1.29...v0.1.30) (2021-03-26)


### Bug Fixes

* rights ([417e1ce](https://gitlab.com/saastack/ui/departments/commit/417e1cea1c7d15ad75e01af1045b3db7107bbaf5))

### [0.1.29](https://gitlab.com/saastack/ui/departments/compare/v0.1.28...v0.1.29) (2021-02-23)


### Bug Fixes

* switch issues ([ddff313](https://gitlab.com/saastack/ui/departments/commit/ddff3131cb7e89db77a899c28f08e2cbcfe8a626))

### [0.1.28](https://gitlab.com/saastack/ui/departments/compare/v0.1.27...v0.1.28) (2021-01-06)

### [0.1.27](https://gitlab.com/saastack/ui/departments/compare/v0.1.26...v0.1.27) (2020-09-09)


### Bug Fixes

* **checklist:** Use selected department's id to check canManage ([b06bb83](https://gitlab.com/saastack/ui/departments/commit/b06bb83f893afd3df36d04e4ce2f1e9336e4f88b))

### [0.1.26](https://gitlab.com/saastack/ui/departments/compare/v0.1.25...v0.1.26) (2020-09-05)


### Bug Fixes

* **checklist:** Implement Roles N Rights ([d7e3987](https://gitlab.com/saastack/ui/departments/commit/d7e3987def14b41517c75e44563f981591b789a0))
* **checklist:** Implement Roles N Rights ([1c28fab](https://gitlab.com/saastack/ui/departments/commit/1c28fab2e35e58fb60a677849b0bd2fb67a9b2c3))
* checklist ([b0866c0](https://gitlab.com/saastack/ui/departments/commit/b0866c041cf27bdf422b041e5e305911b5fff39e))
* description dialog casing ([27faf30](https://gitlab.com/saastack/ui/departments/commit/27faf30101ea2b7c0039b79fdfb4ee4179dbec0b))

### [0.1.25](https://gitlab.com/saastack/ui/departments/compare/v0.1.24...v0.1.25) (2020-08-20)


### Bug Fixes

* **release:** layout change ([726ed44](https://gitlab.com/saastack/ui/departments/commit/726ed44b05bfa6a37b27a41a6226db32b6816a6e))

### [0.1.24](https://gitlab.com/saastack/ui/departments/compare/v0.1.23...v0.1.24) (2020-08-18)


### Bug Fixes

* relay update ([c130cf4](https://gitlab.com/saastack/ui/departments/commit/c130cf40b5bfd360c21187c1364e5120df578260))

### [0.1.23](https://gitlab.com/saastack/ui/departments/compare/v0.1.22...v0.1.23) (2020-08-18)


### Bug Fixes

* relay update ([d4b7739](https://gitlab.com/saastack/ui/departments/commit/d4b7739385a7b8144e5f6500511dfd9ff15980ee))

### [0.1.22](https://gitlab.com/saastack/ui/departments/compare/v0.1.20...v0.1.22) (2020-08-18)


### Bug Fixes

* relay update ([0d54581](https://gitlab.com/saastack/ui/departments/commit/0d54581e4ba01c7905cd1ea841739e1c9e1d7285))
* relay update ([247f750](https://gitlab.com/saastack/ui/departments/commit/247f75000d871b5673e7303bb85a2de9b8b28d82))

### [0.1.21](https://gitlab.com/saastack/ui/departments/compare/v0.1.20...v0.1.21) (2020-08-18)


### Bug Fixes

* relay update ([247f750](https://gitlab.com/saastack/ui/departments/commit/247f75000d871b5673e7303bb85a2de9b8b28d82))

### [0.1.20](https://gitlab.com/saastack/ui/departments/compare/v0.1.19...v0.1.20) (2020-08-13)


### Bug Fixes

* validations ([1913c53](https://gitlab.com/saastack/ui/departments/commit/1913c535ede421692a811cea7365de56325b914b))

### [0.1.19](https://gitlab.com/saastack/ui/departments/compare/v0.1.18...v0.1.19) (2020-08-12)


### Bug Fixes

* casing ([57a6cbd](https://gitlab.com/saastack/ui/departments/commit/57a6cbd6b3a1ec29ca28c2b48a4a897f3333e850))

### [0.1.18](https://gitlab.com/saastack/ui/departments/compare/v0.1.17...v0.1.18) (2020-07-01)


### Bug Fixes

* release issues ([92e9b6d](https://gitlab.com/saastack/ui/departments/commit/92e9b6d642daf0c0bc9ff781f75324556f5ef0bd))

### [0.1.17](https://gitlab.com/saastack/ui/departments/compare/v0.1.16...v0.1.17) (2020-07-01)

### [0.1.16](https://gitlab.com/saastack/ui/departments/compare/v0.1.15...v0.1.16) (2020-04-23)


### Bug Fixes

* wrapper ([1b0158f](https://gitlab.com/saastack/ui/departments/commit/1b0158f62e08e10fb30d5be65948a97180ab6778))

### [0.1.15](https://gitlab.com/saastack/ui/departments/compare/v0.1.14...v0.1.15) (2020-04-18)


### Bug Fixes

* ux ([87d91f6](https://gitlab.com/saastack/ui/departments/commit/87d91f6db9165e041abb5ca0451f7295ca3fd2ec))

### [0.1.14](https://gitlab.com/saastack/ui/departments/compare/v0.1.13...v0.1.14) (2020-04-06)


### Features

* validation rules ([49fce68](https://gitlab.com/saastack/ui/departments/commit/49fce68e6f8113b0d11afed4941facd3d8155b19))

### [0.1.13](https://gitlab.com/saastack/ui/departments/compare/v0.1.11...v0.1.13) (2020-03-28)


### Features

* code reviewed and fixed ([3955b40](https://gitlab.com/saastack/ui/departments/commit/3955b409e3cd65fb02f045f2fecf972526bd1d04))


### Bug Fixes

* **update:** skip ([6cf867f](https://gitlab.com/saastack/ui/departments/commit/6cf867ff35411463503665f72095ac081392fdbe))

### [0.1.12](https://gitlab.com/saastack/ui/departments/compare/v0.1.11...v0.1.12) (2020-03-26)


### Bug Fixes

* **update:** skip ([6cf867f](https://gitlab.com/saastack/ui/departments/commit/6cf867ff35411463503665f72095ac081392fdbe))

### [0.1.11](https://gitlab.com/saastack/ui/departments/compare/v0.1.10...v0.1.11) (2020-03-25)


### Bug Fixes

* **bug fix:** skip ([ec83124](https://gitlab.com/saastack/ui/departments/commit/ec831240fc5c7029f6a947754d8a377433e1031a))

### [0.1.10](https://gitlab.com/saastack/ui/departments/compare/v0.1.9...v0.1.10) (2020-03-23)


### Bug Fixes

* **new ux:** new ux ([48021ad](https://gitlab.com/saastack/ui/departments/commit/48021ad085db7816a4de6523d97ec72b07d6b3bb))

### [0.1.9](https://gitlab.com/saastack/ui/departments/compare/v0.1.8...v0.1.9) (2020-03-19)


### Bug Fixes

* **release:** checklist ([448197d](https://gitlab.com/saastack/ui/departments/commit/448197d4260dd1771bef28286bcaf05c49b1f554))
* **release:** checklist pass ([644e741](https://gitlab.com/saastack/ui/departments/commit/644e74120810e9ec308b77c1f158e2be3ae3f894))

### [0.1.8](https://gitlab.com/saastack/ui/departments/compare/v0.1.7...v0.1.8) (2020-03-07)


### Bug Fixes

* code fixed according to checklist ([2e5e5f6](https://gitlab.com/saastack/ui/departments/commit/2e5e5f6eb36780cf2f0103b64265230ae971e3bd))

### [0.1.7](https://gitlab.com/saastack/ui/departments/compare/v0.1.6...v0.1.7) (2020-03-05)


### Bug Fixes

* breadcrumbs ([74afab5](https://gitlab.com/saastack/ui/departments/commit/74afab5ae6910a272f4c71d8cfb3babe042bdec8))

### [0.1.6](https://gitlab.com/saastack/ui/departments/compare/v0.1.5...v0.1.6) (2020-03-05)


### Bug Fixes

* breadcrumb ([ff5e7fe](https://gitlab.com/saastack/ui/departments/commit/ff5e7fe734000b9b284e8e80474224abbd5f7096))
* delete ([8350bc5](https://gitlab.com/saastack/ui/departments/commit/8350bc5f2e1b3a3616d3d67fa4e06c7f8e83e0a3))
* list should not be clickable ([f50088b](https://gitlab.com/saastack/ui/departments/commit/f50088b92eb553236e70cfb2439ce1da19aef4e0))

### [0.1.5](https://gitlab.com/saastack/ui/departments/compare/v0.1.4...v0.1.5) (2020-02-15)


### Bug Fixes

* added loading to with query ([e01a49e](https://gitlab.com/saastack/ui/departments/commit/e01a49e32d4d5440ee85677642ad87adfde679e4))

### [0.1.4](https://gitlab.com/saastack/ui/departments/compare/v0.1.3...v0.1.4) (2020-02-15)


### Bug Fixes

* versions ([95eb0d5](https://gitlab.com/saastack/ui/departments/commit/95eb0d59d8d55702e9b0de74c3503270dbf64fd4))

### [0.1.3](https://gitlab.com/saastack/ui/departments/compare/v0.1.2...v0.1.3) (2020-02-13)


### Bug Fixes

* hook registration ([8702d37](https://gitlab.com/saastack/ui/departments/commit/8702d37a3918e51e4a8c3d4e551ec8f3e96965f5))

### [0.1.2](https://gitlab.com/saastack/ui/departments/compare/v0.1.1...v0.1.2) (2020-02-13)


### Bug Fixes

* register hook and npm run update ([9edf500](https://gitlab.com/saastack/ui/departments/commit/9edf500d6e8896962e72ac40b00a097404e592aa))

### 0.1.1 (2020-02-12)


### Features

* migrated to saastack ([3091820](https://gitlab.com/saastack/ui/departments/commit/309182028741c43c547efc17a5d54f882dea56fb))
