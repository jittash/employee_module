
### Check List ###

 - [x] UX Match
 - [x] Roles & Rights (Update working on Editor role)
 - [ ] <s>License</s>
 - [ ] <s>Internationalization</s>
 - [ ] <s>Customer Portal Widget</s>
 - [ ] <s>My Space Widget</s>
 - [ ] <s>Reports</s>
 - [ ] <s>Notifications</s>
 - [ ] <s>Notes</s>
 - [ ] <s>Policies</s>
 - [x] Responsive
 - [x] Accessibility
 - [ ] <s>Activity Log</s>
 - [ ] <s>Backward compatibility</s>
 - [ ] <s>Check breadcrumb</s>
 - [x] Check app icon in hook register
 - [x] Remove console log
 - [ ] <s>Check search bar if used in app (use debounce)</s>
 - [x] Check casing of words
 - [x] Check for validation, internationalization in validation messages
 - [ ] <s>Default value check in form</s>
 - [ ] <s>Active app hooks</s>
 - [ ] <s>Default app</s>
 - [x] Check casing in hook
 - [x] Folder structure
 - [x] Remove optimistic updater from delete
 - [ ] <s>IconButton wrap in tooltip</s>
 - [x] Re-fetch on parent change
 - [x] useEffect vs useDidMountEffect 
 - [x] Rename ExpansionPanel to Accordion
 - [ ] <s>Font size should be used from typography</s>
 
                