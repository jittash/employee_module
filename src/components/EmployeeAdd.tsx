import { Trans } from "@lingui/macro";
import { FormContainer } from "@saastack/layouts/containers";
import { useNavigate } from "@saastack/router";
import React, { useState } from "react";
import { useRelayEnvironment } from "react-relay/hooks";
import { useAlert } from "@saastack/core";
import { graphql, Variables } from "relay-runtime";
import EmployeeAddFormComponent from "../forms/EmployeeAddFormComponent";
import CreateEmployeeMutation from "../mutations/CreateEmployeeMutation";
import EmployeeAddInitialValues from "../utils/EmployeeAddInitialValues";
import EmployeeAddValidations from "../utils/EmployeeAddValidations";
import { PubSub } from "@saastack/pubsub";
import { EmployeeInput, InviteEmployeeInput } from "../__generated__/CreateEmployeeMutation.graphql";
import { FormContainerProps } from "@saastack/layouts";
import namespace from "../namespace";
import { createFragmentContainer } from "react-relay";

interface Props extends Omit<FormContainerProps,'formId'> {
    variables: Variables;
    refetch: () => void;
    employees: any
}
const formId = "employee-add-form";

const EmployeeAdd: React.FC<Props> = ({variables,refetch,employees,...props}) => {

    const environment = useRelayEnvironment();
    const navigate = useNavigate();
    const showAlert = useAlert();
    
    const [open, setOpen] = useState<boolean>(true)
    const [loading, setLoading] = useState<boolean>(false)
    const handleClose = () => setOpen(false)
    const navigateBack = () => {
        navigate("../");
    }
    const handleSubmit = (values:EmployeeInput) =>{
        setLoading(true);
        const employee={
            ...values
        }
        CreateEmployeeMutation.commit(environment,variables,employee,{onSuccess,onError})
    }
    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = (response: EmployeeInput) => {
        refetch?.()
        //PubSub.publish(namespace.create, response);
        setLoading(false);
        showAlert(<Trans>Employee added successfully!</Trans>, {
            variant: 'info',
        })
        handleClose()
    }



    const initialValues:EmployeeInput={
        ...EmployeeAddInitialValues
    }
    return (
        <FormContainer
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            header={ <Trans>New Employee</Trans>}
            loading={loading}
            formId={formId}
            {...props} 
            >
            <EmployeeAddFormComponent
                onSubmit={handleSubmit}
                id={formId}
                add
                initialValues={initialValues}
                validationSchema={EmployeeAddValidations}
            />
        </ FormContainer>
    )
}
export default createFragmentContainer(EmployeeAdd, {
    employees: graphql`
        fragment EmployeeAdd_employees on Employee @relay(plural: true) {
            id
        }
    `
})