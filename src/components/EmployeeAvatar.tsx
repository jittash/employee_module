import { Trans } from '@lingui/macro'
import loadable from '@loadable/component'
import { Box, Chip, IconButton, Theme, Tooltip } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { Avatar } from '@saastack/components'
import { UploadResponse } from '@saastack/components/Upload'
import deserialize from '@saastack/utils/deserialize'
import { t } from '@lingui/macro'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import { EmployeeAvatar_employee } from '../__generated__/EmployeeAvatar_employee.graphql'
import UpdateEmployeeMutation from '../mutations/UpdateEmployeeMutation'
import Roles from '@saastack/core/roles/Roles'
import { useCan } from '@saastack/core/roles'
import { useConfig } from '@saastack/core'
import { useI18n } from '@saastack/i18n'

const Upload = loadable(() => import('@saastack/components/Upload'))

interface Props {
    employee: EmployeeAvatar_employee
    size?: 'small' | 'medium'
}

const useStyles = makeStyles(({ spacing, shadows, breakpoints: { up } }: Theme) => ({
    chip: {
        height: 13,
        fontSize: 9,
        lineHeight: 1,
        opacity: 0.9,
        borderRadius: 2,
        textTransform: 'uppercase',
        padding: spacing(0, 0.5),
        boxShadow: shadows[1],
        fontWeight: 500,
        position: 'absolute',
        bottom: 0,
        left: '50%',
        transform: 'translateX(-50%)',
        maxWidth: '100%',
        '& .MuiChip-label': {
            padding: 0,
            textOverflow: 'clip'
        }
    },
    root: {
        position: 'relative',
        // minWidth: spacing(8),
        textAlign: 'center'
    },
    small: {},
    medium: {
        [up('sm')]: {
            width: 44,
            height: 44
        },
        [up('md')]: {
            width: 48,
            height: 48
        }
    }
}))

const EmployeeAvatar: React.FC<Props> = ({ employee, size = 'small' }) => {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false)
    const environment = useRelayEnvironment()
    const can = useCan()
    const { companyId, locationId } = useConfig()
    const isEditor = React.useMemo(() => {
        const roles = [Roles.EmployeesAdmin, Roles.EmployeeSettingsAdmin, Roles.EmployeesEditor]
        return can(roles, companyId!) || can(roles, locationId!)
    }, [companyId, locationId])
    const { i18n } = useI18n()

    const handleOpen = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation()
        if (isEditor) {
            setOpen(true)
        }
    }
    const handleClose = () => setOpen(false)

    const handleUpload = (profileImage: UploadResponse | UploadResponse[]) =>
        UpdateEmployeeMutation.commit(
            environment,
            {
                ...employee,
                profileImage: profileImage as UploadResponse
            },
            ['profileImage'],
            { onSuccess: handleClose }
        )
    const image = employee.profileImage && employee.profileImage.thumbImage
    const { color } = deserialize(employee.metadata as string)
    const handleStopPropagation = (e: React.MouseEvent<HTMLButtonElement>) => e.stopPropagation()
    return (
        <Box className={classes.root} onClick={handleStopPropagation}>
            {open && (
                <Upload
                    src={image as string}
                    onComplete={handleUpload}
                    open={open}
                    onClose={handleClose}
                />
            )}
            <Tooltip title={<Trans>Upload Image</Trans>}>
                <IconButton aria-label={i18n._(t`Upload Image`)} size="small" onClick={handleOpen}>
                    <Avatar
                        alt={i18n._(t`Employee Image`)}
                        className={classes[size]}
                        color={color}
                        src={image as string}
                        title={`${employee.firstName || employee.email}`}
                    />
                </IconButton>
            </Tooltip>
            {employee.designation?.name && (
                <Chip
                    className={classes.chip}
                    size="small"
                    label={employee.designation?.name}
                    color="primary"
                />
            )}
        </Box>
    )
}

export default createFragmentContainer(EmployeeAvatar, {
    employee: graphql`
        fragment EmployeeAvatar_employee on Employee {
            id
            firstName
            email
            profileImage {
                largeImage
                thumbImage
            }
            designation {
                name
            }
            metadata
        }
    `
})
