import { Trans } from '@lingui/macro'
import { useAlert, useAlias, useConfig } from '@saastack/core'
import { ConfirmContainerProps } from '@saastack/layouts'
import { ConfirmContainer } from '@saastack/layouts/containers'
import { useNavigate, useParams } from '@saastack/router'
import React from 'react'
import hooks from "@saastack/hooks"
import { useRelayEnvironment } from 'react-relay/hooks'
import { Variables } from 'relay-runtime'
import DeleteEmployeeMutation from '../mutations/DeleteEmployeeMutation'
import { PubSub } from '@saastack/pubsub'
import namespace from '../namespace'

interface Props extends Omit<ConfirmContainerProps, 'heading' | 'message' | 'onAction'> {
    variables: Variables
    refetch:()=>void
}

const EmployeeDelete: React.FC<Props> = ({ variables,refetch, ...props }) => {
    const alias = useAlias('Employees', { singular: 'Staff', plural: 'Staff' })
    const environment = useRelayEnvironment()
    const [open, setOpen] = React.useState(true)
    const showAlert = useAlert()
    const [loading, setLoading] = React.useState(false)
    const { id: param } = useParams()
    const id = window.atob(param!)

    const navigate = useNavigate()
    //const deleteHooks = hooks.employee.delete.getAllHooks(activeApps)
    const initialValues = {}
    /*deleteHooks.map((H) => {
        set(initialValues, H.fieldName, H.initialValues)
    })*/

    const handleSubmit = () => {
        setLoading(true)
        DeleteEmployeeMutation.commit(environment, id,{onSuccess,onError})
    }

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error'
        })
    }

    const onSuccess = (id: string) => {
        //PubSub.publish(namespace.delete,id)
        setLoading(false)
        refetch()
        showAlert(<Trans>{alias?.singular} deleted successfully.</Trans>, {
            variant: 'info'
        })
        handleClose()
    }
    return (
        <ConfirmContainer
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            loading={loading}
            header={<Trans>Delete {alias?.singular}</Trans>}
            onAction={handleSubmit}
            {...props}
        >
        </ConfirmContainer>
    )
}

export default EmployeeDelete
