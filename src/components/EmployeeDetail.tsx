import React, { useEffect, useState } from "react"
import { DetailContainer } from "@saastack/layouts/containers"
import { createFragmentContainer, graphql } from "react-relay"
import { ActionItem } from "@saastack/components/Actions"
import { BlockOutlined, CloseOutlined, DeleteOutlined, EditOutlined } from "@material-ui/icons"
import { useNavigate, useParams } from "@saastack/router"
import { Trans } from "@lingui/macro"
import { useRelayEnvironment } from "relay-hooks"
import { EmployeeDetail_employees } from "../__generated__/EmployeeDetail_employees.graphql";
import { Box, CardHeader, Typography } from "@material-ui/core"
import { DetailContainerProps } from "@saastack/layouts"
import EmployeeAvatar from "./EmployeeAvatar"

export interface Props extends DetailContainerProps {
    employees: EmployeeDetail_employees;
    //parent: string;
    refetch: () => void;
}

const EmployeeDetail: React.FC<Props> = (props) => {
    const navigate = useNavigate();
    const environment = useRelayEnvironment();
    const [open, setOpen] = useState<boolean>(true)

    /*Fetching ID of employee from URL*/
    const { id: param } = useParams()
    /*Detail of Employee with the given ID*/
    const employee = props.employees.find((i) => i.id === window.atob(param!))

    const navigateBack = ()=> navigate("../")

    const handleClose = (): void => {
        setOpen(false);
        navigateBack()
    }

    useEffect(() => {
        if(!employee){
            navigateBack()
        }
    }, [employee])

    if(!employee){
        return null
    }

    const actions: ActionItem[] = [
        {
            title: <Trans>Update</Trans>,
            icon: EditOutlined,
            onClick: () => navigate("update"),
        },
        {
            title: <Trans>Delete</Trans>,
            icon: DeleteOutlined,
            onClick: () => navigate("delete"),
        },
        {
            title: <Trans>Close</Trans>,
            icon: CloseOutlined,
            onClick: handleClose,
        },
    ]

    const header = (
        <CardHeader
            titleTypographyProps={{ variant: 'h5' }}
            title={
                <>
                    {employee.firstName} {employee.lastName}{' '}
                </>
            }
            avatar={<EmployeeAvatar size="medium" employee={employee} />}
            subheader={
                <>
                    {employee.email}
                   
                    {employee.email && employee.phoneNumber && <span className="dot" />}{' '}
                    {employee.phoneNumber}
                    <Box>
                        {!employee.active && (
                            <Typography variant="body1" color="textPrimary" component={'span'}>
                                <BlockOutlined fontSize="small" /> &nbsp;
                                <div>
                                    <Trans>Inactive</Trans>
                                </div>
                            </Typography>
                        )}
                    </Box>
                </>
            }
        />
    )


    return (
        <DetailContainer
            boxed
            open={open}
            onClose={handleClose}
            actions={actions}
            header={header}
            {...props}
        />
    )
}

export default createFragmentContainer(EmployeeDetail, {
    employees: graphql`
    fragment EmployeeDetail_employees on Employee @relay(plural:true){
        id
        firstName
        lastName
        email
        phoneNumber
        isInvitationAccepted

        userId
        staffProfiles {
            id
            profileType
        }
        userId
        roles {
            role {
                levelId
                roleId
                role {
                    id
                    roleName
                    isDefault
                    priority
                    level
                    moduleRoles {
                        name
                    }
                }
            levelDetails {
                    ... on Location {
                        id
                    }
                    ... on Company {
                        id
                    }
                    ... on Group {
                        id
                    }
                }
            }
        }
        active
        ...EmployeeAvatar_employee
    }
  `
})