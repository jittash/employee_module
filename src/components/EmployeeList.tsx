import React from "react";
import { createFragmentContainer, Disposable, graphql } from "react-relay";
import { EmployeeList_employees } from "../__generated__/EmployeeList_employees.graphql";
import {ListContainer}  from "@saastack/layouts/containers";
import {ListItemAvatar,ListItemText} from "@material-ui/core";
import {TableCellType} from "@saastack/components/Table";
import EmployeeAvatar from "./EmployeeAvatar";
import {Trans} from "@lingui/macro";
import { ListContainerProps } from "@saastack/layouts";

interface Props extends Omit<ListContainerProps<EmployeeList_employees>,"items">{
    //highlighted:string,
    //hasMore:()=>Boolean,
    //isLoading:()=>Boolean,
    //loadMore:()=>void,
    employees:EmployeeList_employees,
    //onItemClick:(id:string)=>void,
    //selectable:boolean,
    //selected:string[],
    //onSelect:(ids:string[])=>void
}

const EmployeeList:React.FC<Props> = ({employees,...props}) => {

    const listProps = {
        render: (e: EmployeeList_employees[0]) => {
            return [
                <ListItemAvatar key="avatar">
                    <EmployeeAvatar employee={e} />
                </ListItemAvatar>,
                <ListItemText
                    key="name"
                    primary={
                        <>
                            {e.firstName} {e.lastName}
                        </>
                    }
                    secondary={e.email}
                />
            ]
        }
    }

    let render: Record<string, (item: any) => React.ReactNode> = {}

    const tableProps = {
        config: [
            {
                key: 'avatar',
                filterLabel: <Trans>Image</Trans>
            },
            {
                key: 'name',
                label: <Trans>Name</Trans>
            },
            {
                key: 'email',
                label: <Trans>Email</Trans>
            },
           {
                key: 'phoneNumber',
                label: <Trans>Phone Number</Trans>
            },
            {
                key: 'department.name',
                label: <Trans>Department</Trans>
            },
            {
                key: 'designation.name',
                label: <Trans>Designation</Trans>
            },
            {
                key: 'isInvitationAccepted',
                hideFilter: true,
                type: TableCellType.Actions
            },
        ],
        render: {
            avatar: (e: EmployeeList_employees[0]) => <EmployeeAvatar employee={e} />,
            name: (e: EmployeeList_employees[0]) => (
                <>
                    {e.firstName} {e.lastName}
                </>
            ),
            isInvitationAccepted: (e: EmployeeList_employees[0]) =>
                e.isInvitationAccepted ? <Trans>Accepted</Trans> : <Trans>Invited</Trans>,
            ...render
        },
        //visibleColumns,
        //onVisibleColumnsChange,
        hoverCheckbox: true
    }
    return(
    <ListContainer<EmployeeList_employees> 
    listProps={listProps}
    tableProps={tableProps}
    items={employees}
    virtual
    {...props}
    />
    )
}

export default createFragmentContainer(EmployeeList,{
    employees:graphql`
        fragment EmployeeList_employees on Employee @relay(plural:true){
            id
            firstName
            lastName
            email
            phoneNumber
            active
            department{
                name
            }
            designation{
                name
            }
            isInvitationAccepted
            staffProfiles{
                id
                profileType
            }
            userId
            ...EmployeeAvatar_employee
        }
    `
}
    
)