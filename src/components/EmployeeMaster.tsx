import { Trans } from '@lingui/macro';
import { AddOutlined } from '@material-ui/icons';
import { ActionItem } from '@saastack/components/Actions';
import { useAlias, useConfig } from '@saastack/core';
import { Layout, LayoutProps } from '@saastack/layouts';
import { useNavigate, Route, Routes, useRouteMatch } from '@saastack/router';
import { useDidMountEffect } from '@saastack/utils';
import React, { Fragment, useState } from 'react';
import { createPaginationContainer, graphql, RelayPaginationProp } from 'react-relay';
import { EmployeeMaster_employees } from '../__generated__/EmployeeMaster_employees.graphql';
import EmployeeAdd from './EmployeeAdd';
import EmployeeDelete from './EmployeeDelete';
import EmployeeDetail from './EmployeeDetail';
import EmployeeList from './EmployeeList';
import EmployeeUpdate from './EmployeeUpdate';
import useForceUpdate from 'use-force-update';

interface Props {
    employees: EmployeeMaster_employees
    parent: string;
    relay: RelayPaginationProp
    layoutProps: LayoutProps
}

const EmployeeMaster: React.FC<Props> = ({
    employees: {
        employees: { edges: employeeEdges }
    },
    parent,
    relay: { hasMore, isLoading, loadMore, refetchConnection },
    layoutProps
}) => {

    const { locations, companyId, locationId, activeApps,customerId } = useConfig()
    /* Alias */
    const alias = useAlias('Employees', { singular: 'Staff', plural: 'Staff' })

    const [selected, setSelected] = useState<string[]>([])

    /* useRouteMatch Hook */
    const match = useRouteMatch<{ id: string }>(':id')
    let highlighted = ''
    if (match?.params.id) {
        try {
            highlighted = window.atob(match?.params.id)
        } catch (e) { }
    }

    const variables = { parent }
    const forceUpdate = useForceUpdate();

    /*Extracting Employees*/
    const passedEmployees = employeeEdges?.map((s) => s.node!) ?? []

    /*useNavigate Hook */
    const navigate = useNavigate();

    const actions: ActionItem[] = [
        {
            title: <Trans>Add</Trans>,
            icon: AddOutlined,
            onClick: () => navigate("add"),
        }
    ]

    /*Whether more data be loaded or not*/
    const _loadMore = () => {
        if (!hasMore() || isLoading()) {
            return
        }
        loadMore(20,()=>forceUpdate())
        forceUpdate()
    }

    const refetchEmployees = () => {
        refetchConnection(20, () => forceUpdate(), { parent }),
        forceUpdate();
    }

    useDidMountEffect(() => refetchEmployees(), [])

    /*Fetching EmployeeList Component in col1,col1 contains all employees as an array*/
    const col1 = <EmployeeList
        highlighted={highlighted}
        hasMore={hasMore}
        isLoading={isLoading}
        loadMore={_loadMore}
        employees={passedEmployees}
        onItemClick={(id: string) => navigate(window.btoa(id || ''))}
        selectable
        selected={selected}
        onSelect={setSelected}
    />

    const header = <Trans>Employees</Trans>

    return (
        <Layout {...layoutProps} col1={col1} actions={actions} header={header}>
            <Routes>
                <Route path="add" element={<EmployeeAdd variables={variables} employees={passedEmployees} refetch={refetchEmployees} />} />
                <Route path=":id" element={<EmployeeDetail parent={parent} employees={passedEmployees} refetch={refetchEmployees} />} />
                <Route path="delete" element={<EmployeeDelete variables={variables} />} />
                <Route path="update" element={<EmployeeUpdate employees={passedEmployees} />} />
            </Routes>
        </Layout>
    )
}

export default createPaginationContainer(
    EmployeeMaster,
    {
        employees: graphql`
            fragment EmployeeMaster_employees on Query 
            @argumentDefinitions(
                count:{type:"Int",defaultValue:20},
                cursor:{type:"String"},
                parent:{type:"String"},    
            ){
                employees(
                    first:$count
                    after:$cursor
                    parent:$parent
                    ) 
                    @connection(key:"EmployeeMaster_employees",filters:[]){
                    edges{
                        cursor
                        node{ 
                            id
                            ...EmployeeList_employees
                            ...EmployeeDetail_employees
                            ...EmployeeUpdate_employees
                            ...EmployeeAdd_employees
                        }    
                    }
                    pageInfo{
                        endCursor
                        hasNextPage
                        hasPreviousPage
                        startCursor
                    }
                }
            }
        `
    },
    {
        direction: "forward",
        getVariables(props, { count, cursor }, fragmentVariables) {
            return {
                count,
                cursor,
                parent: fragmentVariables.parent
            }
        },
        query: graphql`
    # Pagination query to be fetched upon calling 'loadMore'.
    # Notice that we re-use our fragment, and the shape of this query matches our fragment spec
        query EmployeeMasterPaginationQuery(
            $count:Int!
            $cursor:String
            $parent:String!
        ){
            ...EmployeeMaster_employees 
            @arguments(count:$count,cursor:$cursor,parent:$parent)
        }
    `
    },
)