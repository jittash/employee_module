import { Trans } from "@lingui/macro";
import { useAlias, useAlert, Mutable } from "@saastack/core";
import { FormContainerProps } from "@saastack/layouts";
import { FormContainer } from "@saastack/layouts/containers";
import { useNavigate, useParams } from "@saastack/router";
import React, { useEffect } from "react";
import { createFragmentContainer, graphql } from "react-relay";
import { useRelayEnvironment } from "react-relay/hooks";
import EmployeeUpdateFormComponent from "../forms/EmployeeUpdateFormComponent";
import UpdateEmployeeMutation from "../mutations/UpdateEmployeeMutation";
import EmployeeAddValidations from "../utils/EmployeeAddValidations";
import { EmployeeInput } from "../__generated__/CreateEmployeeMutation.graphql";
import { EmployeeUpdate_employees } from "../__generated__/EmployeeUpdate_employees.graphql";

interface Props extends Omit<FormContainerProps, 'formId'> {
    employees: EmployeeUpdate_employees
    hideEmployeePhoneNumber: Boolean
}

const formId = 'employee-update-form'

const EmployeeUpdate: React.FC<Props> = ({ employees, hideEmployeePhoneNumber, ...props }) => {
    const navigate = useNavigate()
    const alias = useAlias('Employees', { singular: 'Staff', plural: 'Staff' })

    const showAlert = useAlert()
    const [loading, setLoading] = React.useState(false)
    const [open, setOpen] = React.useState(true)
    const environment = useRelayEnvironment()

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)

    const { id } = useParams()
    const employee = employees.find((i) => i.id === window.atob(id!))
    useEffect(() => {
        if (!employee) {
            handleClose()
        }
    }, [employee])
    if (!employee) {
        return null
    }

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error'
        })
    }
    const onSuccess = () => {
        setLoading(false)
        if (props.onClose) {
            props.onClose()
        }
        handleClose()
    }
    const handleSubmit = (values: EmployeeInput ) => {
        setLoading(true)
        
        UpdateEmployeeMutation.commit(
            environment,
            employee,
            ['firstName', 'lastName', 'phoneNumber'],
            {
                onSuccess,
                onError
            },
         
        )
    }

    /*const initialValues = {
        ...employee,
        name: `${employee.firstName} ${employee.lastName}`.trim()
    }*/

    const initialValues = employee as Mutable<EmployeeUpdate_employees[0]>

    return (
        <FormContainer
            onExited={navigateBack}
            open={open}
            onClose={handleClose}
            header={<Trans>Update {alias?.singular}</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <EmployeeUpdateFormComponent
                onSubmit={handleSubmit}
                initialValues={initialValues}
                id={formId}
                validationSchema={EmployeeAddValidations}
                enableReinitialize={false}

            />
        </FormContainer>
    )
}


export default createFragmentContainer(EmployeeUpdate, {
    employees: graphql`
        fragment EmployeeUpdate_employees on Employee @relay(plural: true) {
            id
            firstName
            lastName
            email
            phoneNumber
        }
    `,
})
