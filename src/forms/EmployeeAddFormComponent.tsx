import { Grid } from "@material-ui/core";
import { useConfig } from "@saastack/core";
import { Form, Input, PhoneNumber } from "@saastack/forms";
import { FormProps } from "@saastack/forms/types";
import React from "react";
import { EmployeeInput } from "../__generated__/UpdateEmployeeMutation.graphql";

export interface Props extends FormProps<EmployeeInput> {
    onSubmit:(values:EmployeeInput)=>void;
    id:any;
    initialValues:any;
    validationSchema:any;
    add?:boolean
}

const EmployeeAddFormComponent: React.FC<Props> = ({add,...props}) => {
    const {company} = useConfig()
    return (
        <Form {...props}>
            <Input variant="standard" name="name" placeholder="Name" grid={{xs:12}} />
            <Input variant="standard" name="email" placeholder="Email" grid={{xs:12}}/>
            <PhoneNumber defaultCountry={company?.address?.country} grid={{xs:12}}/>
        </Form>

    );
}

export default EmployeeAddFormComponent;