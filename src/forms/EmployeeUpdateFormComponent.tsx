/*import { Trans } from '@lingui/macro'
import { useConfig } from '@saastack/core'
import { Form, PhoneNumber } from '@saastack/forms'
import Input from '@saastack/forms/Input'
import { FormProps } from '@saastack/forms/types'
import { useQuery } from '@saastack/relay'
import React from 'react'
import { graphql } from 'react-relay'

export interface Props extends FormProps {
    hidePhoneNumber: Boolean
}



const EmployeeUpdateFormComponent: React.FC<Props> = (props) => {
    const { company } = useConfig()
    const { hidePhoneNumber } = props


    return (
        <Form {...props}>
            <Input variant="standard" name="name" label={<Trans>Name</Trans>} grid={{ xs: 12 }} />
            <Input
                variant="standard"
                disabled
                name="email"
                label={<Trans>Email</Trans>}
                grid={{ xs: 12 }}
            />
            {!hidePhoneNumber && <PhoneNumber
                variant="standard"
                name="phoneNumber"
                label={<Trans>Mobile</Trans>}
                grid={{ xs: 12 }}
                defaultCountry={
                    (!props?.initialValues?.phoneNumber && company?.address?.country) as string
                }
            />}
        </Form>
    )
}


*/


import EmployeeAddFormComponent from "./EmployeeAddFormComponent";

const EmployeeUpdateFormComponent = EmployeeAddFormComponent;

export default EmployeeUpdateFormComponent;