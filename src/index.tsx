import { t, Trans } from '@lingui/macro'
import { PermContactCalendarOutlined } from '@material-ui/icons'
import { Roles } from '@saastack/core/roles'
import hooks from '@saastack/hooks'
import { PubSub } from '@saastack/pubsub'
import React from 'react'
import namespace from './namespace'
import EmployeePage from './pages/EmployeePage'

PubSub.register(Object.values(namespace))
let loaded = false

const load = () => {
    if (loaded) {
        return
    }
    hooks.settings.items.registerHook('departments', {
        component: EmployeePage,
        icon: PermContactCalendarOutlined,
        path: 'departments',
        title: t`Departments`,
        parent: 'masters',
        role: [Roles.DepartmentsAdmin, Roles.DepartmentsEditor, Roles.DepartmentsViewer],
        level: 'com',
        appName: 'Departments',
        useAlias: true,
    })
    hooks.alias.list.registerHook('department', {
        appName: 'Departments',
        priority: 5,
        header: <Trans>What is the entity called which has divisions of your organization?</Trans>,
        subHeader: <Trans>Ex. Department, sector etc</Trans>,
        initialValue: {
            singular: 'Department',
            plural: 'Departments',
        },
    })
    loaded = true
}

load()
