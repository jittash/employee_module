import { MutationCallbacks, setNodeValue } from "@saastack/relay";
import { deserialize } from "@saastack/utils";
import { ConnectionHandler, Disposable, Environment, RecordProxy, RecordSourceSelectorProxy, Variables } from "relay-runtime";
import { commitMutation,graphql } from "react-relay"
import {
    CreateEmployeeMutation,
    CreateEmployeeMutationResponse,
    InviteEmployeeInput,
} from "../__generated__/CreateEmployeeMutation.graphql"

const mutation = graphql`
    mutation CreateEmployeeMutation($input:InviteEmployeeInput){
        inviteEmployee(input:$input){
            clientMutationId
            payload{
                employees{
                    id
                    firstName
                    lastName
                    active
                    email
                    phoneNumber
                    departmentId
                    designationId
                    department {
                        name
                    }
                    designation {
                        name
                    }
                    isInvitationAccepted
                    profileImage {
                        largeImage
                        thumbImage
                    }
                    metadata
                    roles {
                        role {
                            levelId
                            roleId
                            role {
                                id
                                roleName
                                isDefault
                                priority
                            }
                        }
                    }
                    staffProfiles {
                        id
                        profileType
                    }
                }
            }
        }
    }
`

const sharedUpdater = (
    store: RecordSourceSelectorProxy,
    node: RecordProxy,
    employee:InviteEmployeeInput,
) => {
    const rootProxy = store.getRoot()
    const connection = ConnectionHandler.getConnection(rootProxy, 'EmployeeMaster_employees')
    if (connection) {
        const newEdge = ConnectionHandler.createEdge(store, connection, node, 'EmployeeNode')
        ConnectionHandler.insertEdgeBefore(connection, newEdge)
    }
}

let tempID=0
const commit = (
    environment: Environment,
    variables: Variables,
    employee: InviteEmployeeInput,
    callbacks: MutationCallbacks<any>
): Disposable => {
    const input: InviteEmployeeInput = {
        parent:variables.parent,
        employee,
        clientMutationId: `${tempID++}`
    }


    return commitMutation<CreateEmployeeMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        //employee:EmployeeInput,
        optimisticUpdater: (store: RecordSourceSelectorProxy) => {
            const id = `client:newService:${tempID++}`
            const node = store.create(id, 'Employee')
            setNodeValue(store, node, employee)
            sharedUpdater(store, node, employee)
        },

        
        updater: (store: RecordSourceSelectorProxy) => {
            const payload = store.getRootField('inviteEmployee')
            const node1 = payload!.getLinkedRecord('payload')
            const records = node1!.getLinkedRecords('employees')

            if (records?.length) {
                const node = records[0]
                if (node) {
                    const meta = node.getValue('metadata') as string
                    const metadata = deserialize(meta)
                    const newUser = metadata.new
                    if (!newUser) {
                        return
                    }
                }
                sharedUpdater(store, node,employee)
            }

        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CreateEmployeeMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({
                    ...employee.employees![0],
                    ...response.inviteEmployee.payload!.employees[0]
                })
            }
        }
    })

}
export default { commit }