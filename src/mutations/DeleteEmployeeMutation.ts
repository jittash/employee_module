import { commitMutation, graphql } from 'react-relay'

import {
    ConnectionHandler,
    Disposable,
    Environment,
    RecordSourceSelectorProxy,
    Variables
} from 'relay-runtime'
import { DeleteEmployeeInput, DeleteEmployeeMutation } from '../__generated__/DeleteEmployeeMutation.graphql'

interface Callbacks {
    onError?: (message: string) => void
    onSuccess?: (response: string) => void
}

const mutation = graphql`
    mutation DeleteEmployeeMutation($input: DeleteEmployeeInput) {
        deleteEmployee(input: $input) {
            clientMutationId
        }
    }
`

let tempID = 0

const sharedUpdater = (store: RecordSourceSelectorProxy, id: string) => {
    const rootProxy = store.getRoot()
    const connection = ConnectionHandler.getConnection(rootProxy, 'EmployeeMaster_employees')
    if (connection) {
        ConnectionHandler.deleteNode(connection, id)
    }
}

const commit = (
    environment: Environment,
    id:string,
    callbacks?: Callbacks
): Disposable => {
    const input: DeleteEmployeeInput = {
        id,
        clientMutationId: `${tempID++}`
    }

    return commitMutation<DeleteEmployeeMutation>(environment, {
        mutation,
        variables: {
            input
        },
       updater: (store: RecordSourceSelectorProxy) => sharedUpdater(store, id),
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess(id)
            }
        }
    })
}

export default { commit }
