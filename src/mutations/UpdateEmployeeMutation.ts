import { setNodeValue } from '@saastack/relay'
import { pick } from 'lodash-es'
import { commitMutation, graphql } from 'react-relay'
import { Disposable, Environment, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    EmployeeInput,
    UpdateEmployeeInput,
    UpdateEmployeeMutation,
    UpdateEmployeeMutationResponse
} from '../__generated__/UpdateEmployeeMutation.graphql'

interface Callbacks {
    onError?: (message: string) => void
    onSuccess?: (response: EmployeeInput) => void
}

const mutation = graphql`
    mutation UpdateEmployeeMutation($input: UpdateEmployeeInput) {
        updateEmployee(input: $input) {
            clientMutationId
            payload {
                id
                firstName
                lastName
                active
                email
                phoneNumber
                departmentId
                designationId
                department {
                    name
                }
                designation {
                    name
                }
                isInvitationAccepted
                profileImage {
                    largeImage
                    thumbImage
                }
                metadata
              
                roles {
                    role {
                        levelId
                        roleId
                        role {
                            id
                            roleName
                            isDefault
                            priority
                        }
                    }
                }
                
                staffProfiles {
                    id
                    
                    profileType
                }
            }
        }
    }
`

let tempID = 0

const sharedUpdater = (
    store: RecordSourceSelectorProxy,
    employee: EmployeeInput,
    updateMask: string[]
) => {
    if (employee.id) {
        const node = store.get(employee.id)
        if (node) {
            setNodeValue(store, node, pick(employee, updateMask))
        }
    }
}


const commit = (
    environment: Environment,
    employee: EmployeeInput,
    updateMask: string[],
    callbacks?: Callbacks,
): Disposable => {
    const input: UpdateEmployeeInput = {
        updateMask: { paths: updateMask },
        employee,
        clientMutationId: `${tempID++}`,
        //updateRole,
        //locationIds
    }
    return commitMutation<UpdateEmployeeMutation>(environment, {
        mutation,
        variables: {
            input
        },
        optimisticUpdater: (store: RecordSourceSelectorProxy) => {
            /*if (employee.id) {
                const node = store.get(employee.id)
                if (node) {
                    setNodeValue(store, node, pick(employee, updateMask))
                }
            }*/
            sharedUpdater(store, employee, updateMask)
        },
        updater: (store: RecordSourceSelectorProxy) => sharedUpdater(store, employee, updateMask),
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: UpdateEmployeeMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({ ...employee, ...response.updateEmployee.payload })
            }
        }
    })
}

export default { commit }
