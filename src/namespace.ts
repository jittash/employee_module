const namespace = {
    count: 'employee/count',
    create: 'service/create',
    update: 'service/update',
    delete: 'service/delete'
}

export default namespace
