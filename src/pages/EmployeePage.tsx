import { ErrorComponent, Loading } from '@saastack/components'
import { useConfig } from '@saastack/core'
import { useQuery } from '@saastack/relay'
import { useDidMountEffect } from '@saastack/utils'
import React from 'react'
import { graphql } from 'react-relay'
import { EmployeePageQuery } from '../__generated__/EmployeePageQuery.graphql'
import EmployeeMaster from '../components/EmployeeMaster'
import { LayoutProps } from '@saastack/layouts'

const query = graphql`
    query EmployeePageQuery($count:Int!,$cursor:String,$parent:String) {
        ...EmployeeMaster_employees @arguments(count:$count,cursor:$cursor,parent: $parent)
    }
`

interface Props {
    parent?: string,
    layoutProps?: LayoutProps
}

const EmployeePage: React.FC<Props> = props => {
    const { companyId } = useConfig()
    const parent = (props.parent || companyId)!
    const variables: EmployeePageQuery['variables'] = { 
        count:20,
        parent,
    }
    const { data, loading, error, refetch } = useQuery<EmployeePageQuery>(query, variables)
    useDidMountEffect(() => {
        refetch()
    }, [parent])
    if (loading) {
        return <Loading/>
    }
    if (error) {
        return <ErrorComponent error={error}/>
    }

    return <EmployeeMaster  {...props} employees={data!} parent={parent}/>
}

export default EmployeePage
