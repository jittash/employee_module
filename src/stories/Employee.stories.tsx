import { t } from '@lingui/macro'
import { PermContactCalendarOutlined } from '@material-ui/icons'
import { Roles } from '@saastack/core/roles'
import hooks from '@saastack/hooks'
import React from 'react'
import EmployeePage from '../pages/EmployeePage'
import Wrapper from './Wrapper'

/*hooks.app.subApp.registerHook('departments', {
    component: EmployeePage,
    icon: PermContactCalendarOutlined,
    path: 'departments',
    title: t`Departments`,
    role: [Roles.DepartmentsAdmin, Roles.DepartmentsEditor, Roles.DepartmentsViewer],
    level: 'com',
    appName: 'Departments',
})*/

export default {
    title: 'Employees',
    decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
}

export const Default = () => <EmployeePage/>
