import { palette, serialize } from "@saastack/utils";
import { sample } from "lodash-es";

const EmployeeAddInitialValues = {
    isInvitationAccepted:false,
    active:true,
    id:'',
    name:'',
    email:'',
    designation:'',
    departmentId: '',
    language: 'en-US',
    locationIds: [],
    phoneNumber: '',
    metadata: serialize({ color: sample(palette) }),
    enableLogin: true
}
export default EmployeeAddInitialValues;