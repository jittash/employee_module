import { t } from '@lingui/macro'
import * as yup from 'yup'

const EmployeeAddValidations = yup.object().shape({
    name: yup
        .string()
        .required((t`Required` as unknown) as string)
        .max(100, (t`Max 100 characters allowed` as unknown) as string),
    email: yup
        .string()
        .required((t`Required` as unknown) as string)
        .email((t`Invalid email` as unknown) as string)
        .max(100, (t`Max 100 characters allowed` as unknown) as string),
    description: yup.string().max(2000, (t`Max 2000 characters allowed` as unknown) as string),
    locationIds: yup
        .array()
        .of(yup.string())
        .min(1, (t`Required` as unknown) as string)
})

export default EmployeeAddValidations
