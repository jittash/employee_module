import { t } from '@lingui/macro'
import * as yup from 'yup'

const EmployeeUpdateValidations = yup.object().shape({
    name: yup
        .string()
        .required((t`Required` as unknown) as string)
        .max(100, (t`Max 100 characters allowed` as unknown) as string),
    email: yup
        .string()
        .required((t`Required` as unknown) as string)
        .email((t`Invalid email` as unknown) as string)
        .max(100, (t`At most 100 characters allowed` as unknown) as string),
    phoneNumber: yup
        .string()
        .nullable()
        .max(15, (t`At most 15 characters allowed` as unknown) as string)
})

export default EmployeeUpdateValidations
